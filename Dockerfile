FROM alpine:latest

ENV PYTHONUNBUFFERED=1
ENV TZ=US/Mountain
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apk update && \
    apk add --update bash \
    ca-certificates \
    curl \
    docker-cli \
    git \
    gnupg \
    make \
    py3-pip \
    python3 \
    tar \
    zip
